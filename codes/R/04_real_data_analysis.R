# In this script, we plot descriptive statistics of official COVID-19 confirmed cases in the United States.

rm(list = ls())

require(readr)
require(ggplot2)
library(dplyr)
library(ggpubr)
library(lubridate)

# Read and merge data

df <- read_csv('../../data/all-states-history.csv', guess_max = 10000)
df$date <- ymd(df$date)

# Plotting outcome variables per state

temp <- df %>%
  group_by(state, period = floor_date(date, 'week')) %>% 
  summarize(amount = sum(positiveIncrease))

f1 <- ggplot(temp) +
  aes(x = period, y = amount, group = state, color = state) +
  geom_line() +
  xlab('') +
  ylab('New Confirmed Cases (Raw)') +
  scale_x_date(breaks = '2 months', limits = c(min(temp$period), max(temp$period) - months(2))) +
  theme_bw() +
  scale_color_grey() +
  theme(legend.position = "None")

temp <- temp %>% group_by(state) %>% mutate(log_amount = log(1+amount))

f2 <- ggplot(temp) +
  aes(x = period, y = log_amount, group = state, color = state) +
  geom_line() +
  xlab('') +
  ylab('New Confirmed Cases (Logs)') +
  scale_x_date(breaks = '2 months', limits = c(min(temp$period), max(temp$period) - months(2))) +
  theme_bw() +
  scale_color_grey() +
  theme(legend.position = "None")

temp <- temp %>% group_by(state) %>% mutate(delta_log_C = log(1+amount) - log(1+dplyr::lag(amount)))

f3 <- ggplot(temp) +
  aes(x = period, y = delta_log_C, group = state, color = state) +
  geom_line() +
  xlab('') +
  ylab('New Confirmed Cases (Growth Rate)') +
  scale_x_date(breaks = '2 months', limits = c(min(temp$period), max(temp$period) - months(2))) +
  theme_bw() +
  scale_color_grey() +
  theme(legend.position = "None")

ggarrange(f1,f2,f3,
          ncol = 1,
          nrow = 3,
          align = 'v',
          common.legend = F,
          legend = 'none',
          widths = c(1,1),
          heights = c(1,1,1)) +
  ggsave('../../graphs/us_raw_grid.png', width = 20, height = 30, units = "cm")