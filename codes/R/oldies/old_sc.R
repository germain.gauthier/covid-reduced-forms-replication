# In this script, we implement different synthetic control methods on our simulated dataset.

rm(list = ls())

library(gsynth)
library(xtable)

# For graphs in the appendix
my_size = 5
col1 <- ggplot() + annotate(geom = 'text', x=1, y=1, label="Inefficient Policy", size = my_size) + theme_void()
col2 <- ggplot() + annotate(geom = 'text', x=1, y=1, label="Efficient Policy", size = my_size) + theme_void()

# Load simulated dataset

df = readRDS('../../data/rds/SC_simulated_no_effect.RDS')
df2 = readRDS('../../data/rds/SC_simulated_with_effect.RDS')

# Fit Synthetic Controls
myestimator = 'ife'
SC <- gsynth(cumsum ~ trt, data = df, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator)
SC2 <- gsynth(cumsum ~ trt, data = df2, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator)
SC3 <- gsynth(newInfected ~ trt, data = df, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator)
SC4 <- gsynth(newInfected ~ trt, data = df2, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator)

myestimator2 = 'mc'
SC5 <- gsynth(cumsum ~ trt, data = df, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator2)
SC6 <- gsynth(cumsum ~ trt, data = df2, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator2)
SC7 <- gsynth(newInfected ~ trt, data = df, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator2)
SC8 <- gsynth(newInfected ~ trt, data = df2, index = c("dep","time"), se = TRUE, seed = 1234, estimator = myestimator2)

# Computing M metrics for synthetic controls
SC$att.avg
sum(diff(SC$Y.tr))/sum(diff(SC$Y.ct))
SC2$att.avg
sum(diff(SC$Y.tr))/sum(diff(SC2$Y.ct))
SC3$att.avg
sum(SC3$Y.tr)/sum(SC3$Y.ct)
SC4$att.avg
sum(SC3$Y.tr)/sum(SC4$Y.ct)

SC5$att.avg
sum(diff(SC5$Y.tr))/sum(diff(SC5$Y.ct))
SC6$att.avg
sum(diff(SC5$Y.tr))/sum(diff(SC6$Y.ct))
SC7$att.avg
sum(SC7$Y.tr)/sum(SC7$Y.ct)
SC8$att.avg
sum(SC7$Y.tr)/sum(SC8$Y.ct)

# Make table of M Metrics

ATTs <- c(SC$att.avg,
          SC2$att.avg,
          SC3$att.avg,
          SC4$att.avg,
          SC5$att.avg,
          SC6$att.avg,
          SC7$att.avg,
          SC8$att.avg)

Ms <- c(sum(diff(SC$Y.tr))/sum(diff(SC$Y.ct)),
        sum(diff(SC$Y.tr))/sum(diff(SC2$Y.ct)),
        sum(SC3$Y.tr)/sum(SC3$Y.ct),
        sum(SC3$Y.tr)/sum(SC4$Y.ct),
        sum(diff(SC5$Y.tr))/sum(diff(SC5$Y.ct)),
        sum(diff(SC5$Y.tr))/sum(diff(SC6$Y.ct)),
        sum(SC7$Y.tr)/sum(SC7$Y.ct),
        sum(SC7$Y.tr)/sum(SC8$Y.ct))

outcomes <- c('Cumulative', 'Cumulative',
              'New Infected', 'New Infected',
              'Cumulative', 'Cumulative',
              'New Infected', 'New Infected')

methods <- c('GSC',
             'GSC',
             'GSC',
             'GSC',
             'MC',
             'MC',
             'MC',
             'MC')

effects <- c('Efficient', 'Inefficient',
             'Efficient', 'Inefficient',
             'Efficient', 'Inefficient',
             'Efficient', 'Inefficient')

df_scs <- data.frame(methods, outcomes, effects, ATTs, Ms)
colnames(df_scs) <- c('Method', 'Outcome', 'Policy', 'Estimated ATT', 'Metric M')
print(xtable(df_scs, digits = 1), include.rownames=FALSE, file = "../../tables/sc_metrics.tex")

# Generalized Synthetic Control

max_raw = max(max(SC$Y.co),max(SC2$Y.co))
min_effect <- min(c(min(SC$eff), min(SC2$eff)))
max_effect <- max(c(max(SC$eff), max(SC2$eff)))

g1 <- plot(SC, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('New Infected Individuals') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g2 <- plot(SC, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('Estimated Treatment Effect') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g3 <- plot(SC, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('Y(1) and Predicted Y(0)') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw()

g4 <- plot(SC2, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g5 <- plot(SC2, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g6 <- plot(SC2, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw()

max_raw = max(max(SC3$Y.co),max(SC4$Y.co))
min_effect <- min(c(min(SC3$eff), min(SC4$eff)))
max_effect <- max(c(max(SC3$eff), max(SC4$eff)))

g7 <- plot(SC3, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('New Infected Individuals') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g8 <- plot(SC3, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('Estimated Treatment Effect') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g9 <- plot(SC3, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('Y(1) and Predicted Y(0)') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() 

g10 <- plot(SC4, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g11 <- plot(SC4, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g12 <- plot(SC4, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw()

ggarrange(col1, col2, g1, g4, g2, g5, g3, g6, 
          ncol = 2, 
          nrow = 4, 
          align = 'v', 
          common.legend = F, 
          legend = 'bottom',
          widths = c(1,1),
          heights = c(1,5,5,5)) +
  ggsave(paste0('../../graphs/simulated_data_sc_cumulative_',myestimator, '.png'), width = 25, height = 30, units = "cm")

ggarrange(col1, col2, g7, g10, g8, g11, g9, g12, 
          ncol = 2, 
          nrow = 4, 
          align = 'v', 
          common.legend = F, 
          legend = 'bottom',
          widths = c(1,1),
          heights = c(1,5,5,5)) +
  ggsave(paste0('../../graphs/simulated_data_sc_raw_',myestimator, '.png'), width = 25, height = 30, units = "cm")


# Matrix Completion Method

max_raw = max(max(SC5$Y.co),max(SC6$Y.co))
min_effect <- min(c(min(SC5$eff), min(SC6$eff)))
max_effect <- max(c(max(SC5$eff), max(SC6$eff)))

g1 <- plot(SC5, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('New Infected Individuals') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g2 <- plot(SC5, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('Estimated Treatment Effect') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g3 <- plot(SC5, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('Y(1) and Predicted Y(0)') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw()

g4 <- plot(SC6, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g5 <- plot(SC6, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g6 <- plot(SC6, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw()

max_raw = max(max(SC7$Y.co),max(SC8$Y.co))
min_effect <- min(c(min(SC7$eff), min(SC8$eff)))
max_effect <- max(c(max(SC7$eff), max(SC8$eff)))

g7 <- plot(SC7, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('New Infected Individuals') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g8 <- plot(SC7, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('Estimated Treatment Effect') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g9 <- plot(SC7, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('Y(1) and Predicted Y(0)') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() 

g10 <- plot(SC8, theme_bw = F, type = 'raw', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) + 
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw() +
  theme(legend.position = "none")

g11 <- plot(SC8, theme_bw = T, type = 'gap', shade.post = FALSE) + 
  ggtitle('') + ylab('') + xlab('') + ylim(min_effect, max_effect) +
  geom_vline(xintercept = 0, linetype = 'dashed') +
  theme_bw()

g12 <- plot(SC8, theme_bw = T, type = 'counterfactual', shade.post = FALSE) + 
  ggtitle('') + xlab('') + ylab('') + ylim(0, max_raw) +
  geom_vline(xintercept = 19, linetype = 'dashed') +
  theme_bw()

ggarrange(col1, col2, g1, g4, g2, g5, g3, g6, 
          ncol = 2, 
          nrow = 4, 
          align = 'v', 
          common.legend = F, 
          legend = 'bottom',
          widths = c(1,1),
          heights = c(1,5,5,5)) +
  ggsave(paste0('../../graphs/simulated_data_sc_cumulative_',myestimator2, '.png'), width = 25, height = 30, units = "cm")

ggarrange(col1, col2, g7, g10, g8, g11, g9, g12, 
          ncol = 2, 
          nrow = 4, 
          align = 'v', 
          common.legend = F, 
          legend = 'bottom',
          widths = c(1,1),
          heights = c(1,5,5,5)) +
  ggsave(paste0('../../graphs/simulated_data_sc_raw_',myestimator2, '.png'), width = 25, height = 30, units = "cm")
