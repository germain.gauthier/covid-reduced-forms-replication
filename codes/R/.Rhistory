scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g8 <- ggplot(df) +
aes(x = time, y = log(newInfected), color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('Logs') +
ylim(0, max_log) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g9 <- ggplot(df) +
aes(x = time, y = delta_log_I, color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('Growth Rates') +
ylim(min_rate, max_rate) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g10 <- ggplot(df2) +
aes(x = time, y = newInfected, color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('') +
ylim(0, max_raw) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g11 <- ggplot(df2) +
aes(x = time, y = log(newInfected), color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('') +
ylim(0, max_log) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g12 <- ggplot(df2) +
aes(x = time, y = delta_log_I, color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('') +
ylim(min_rate, max_rate) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
ggarrange(col1, col2, g7, g10, g8, g11, g9, g12,
ncol = 2,
nrow = 4,
align = 'v',
common.legend = F,
widths = c(1,1),
heights = c(1,5,5,5)) +
ggsave('../../graphs/simulated_data_event_study.png', width = 25, height = 30, units = "cm")
View(df)
df <- df %>% group_by(dep) %>%
filter(!(any(delta_log_I == -Inf)))
View(df)
View(df2)
df2 <- df2 %>% group_by(dep) %>%
filter(!(any(delta_log_I == -Inf)))
# This script simulates epidemic trajectories assuming a SIRD data-generating process.
# Resulting datasets are saved in .dta format for further analysis using the software Stata,
# and in .RDS format for further analysis using R.
# We also output descriptive graphs so readers may visually inspect resulting epidemic trajectories.
rm(list = ls())
require(ggplot2)
library(dplyr)
library(foreign)
source('simulation_functions.R')
# For graphs in the appendix
my_size = 5
col1 <- ggplot() + annotate(geom = 'text', x=1, y=1, label="Inefficient Policy", size = my_size) + theme_void()
col2 <- ggplot() + annotate(geom = 'text', x=1, y=1, label="Efficient Policy", size = my_size) + theme_void()
# Additional Parameters
# treatment_date = date at which treatment occurs (for the SC and DID)
# end_date = last period considered for simulations
treatment_date = 20
end_date = 50
# Difference-in-Differences Dataset
ngroups = 2
nreps = 149
pop = c(1000000, 10000000)
I_0 = c(100,1000)
fe <- rnorm(2,0,0) # to guarantee parallel trends we set geography fixed effects to zero
thresh = c(151, treatment_date)
df <- make_df(effect = 0, ngroups, end_date, pop, I_0, fe, thresh)
df2 <- make_df(effect = tau, ngroups, end_date, pop, I_0, fe, thresh)
# Output datasets in .dta format
write.dta(df, '../../data/dta/did_simulated_no_effect.dta')
write.dta(df2, '../../data/dta/did_simulated_with_effect.dta')
# Output datasets in .RDS format
saveRDS(df, '../../data/rds/did_simulated_no_effect.RDS')
saveRDS(df2, '../../data/rds/did_simulated_with_effect.RDS')
# To keep all graphs at the same scale
max_raw = max(df$newInfected)
max_log = log(max_raw)
max_rate = max(c(max(df$delta_log_I, na.rm = T), max(df2$delta_log_I, na.rm = T)))
min_rate = min(c(min(df$delta_log_I, na.rm = T), min(df2$delta_log_I, na.rm = T)))
g1 <- ggplot(df) +
aes(x = time, y = newInfected, color = dep, group = dep) +
geom_line() +
geom_vline(aes(xintercept = treatment_date - 1), linetype = 'dashed') +
xlab('') +
ylab('Raw') +
ylim(0, max_raw) +
theme_bw() +
scale_color_grey(name = "", labels = c("Control", "Treated")) +
theme(legend.position = "bottom",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g2 <- ggplot(df) +
aes(x = time, y = log(newInfected), color = dep, group = dep) +
geom_line() +
geom_vline(aes(xintercept = treatment_date - 1), linetype = 'dashed') +
xlab('') +
ylab('Logs') +
ylim(0, max_log) +
theme_bw() +
scale_color_grey(name = "", labels = c("Control", "Treated")) +
theme(legend.position = "bottom",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g3 <- ggplot(df) +
aes(x = time, y = delta_log_I, color = dep, group = dep) +
geom_line() +
geom_vline(aes(xintercept = treatment_date - 1), linetype = 'dashed') +
xlab('') +
ylab('Growth Rates') +
ylim(min_rate, max_rate) +
theme_bw() +
scale_color_grey(name = "", labels = c("Control", "Treated")) +
theme(legend.position = "bottom",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g4 <- ggplot(df2) +
aes(x = time, y = newInfected, color = dep, group = dep) +
geom_line() +
geom_vline(aes(xintercept = treatment_date - 1), linetype = 'dashed') +
xlab('') +
ylab('') +
ylim(0, max_raw) +
theme_bw() +
scale_color_grey(name = "", labels = c("Control", "Treated")) +
theme(legend.position = "bottom",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g5 <- ggplot(df2) +
aes(x = time, y = log(newInfected), color = dep, group = dep) +
geom_line() +
geom_vline(aes(xintercept = treatment_date - 1), linetype = 'dashed') +
xlab('') +
ylab('') +
ylim(0, max_log) +
theme_bw() +
scale_color_grey(name = "", labels = c("Control", "Treated")) +
theme(legend.position = "bottom",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g6 <- ggplot(df2) +
aes(x = time, y = delta_log_I, color = dep, group = dep) +
geom_line() +
geom_vline(aes(xintercept = treatment_date - 1), linetype = 'dashed') +
xlab('') +
ylab('') +
ylim(min_rate, max_rate) +
theme_bw() +
scale_color_grey(name = "", labels = c("Control", "Treated")) +
theme(legend.position = "bottom",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
ggarrange(col1, col2, g1, g4, g2, g5, g3, g6,
ncol = 2,
nrow = 4,
align = 'v',
common.legend = T,
legend = 'bottom',
widths = c(1,1),
heights = c(1,5,5,5)) +
ggsave('../../graphs/simulated_data_did.png', width = 25, height = 30, units = "cm")
# Event Study Dataset
ngroups = 50
nreps = 149
pop = floor(runif(ngroups, 100000, 1000000)) # size of the population
I_0 = floor(runif(ngroups, 0.001, 0.01) * pop) # initial size of the infected population
fe <- rnorm(ngroups,0,0.1)
thresh = floor(runif(ngroups, 0, 50)) # time at which the policy is implemented
df <- make_df(effect = 0, ngroups, end_date, pop, I_0, fe, thresh)
df2 <- make_df(effect = tau, ngroups, end_date, pop, I_0, fe, thresh)
df2 <- df2 %>% group_by(dep) %>%
filter(delta_log_I != -Inf)
df <- df %>% group_by(dep) %>%
filter(delta_log_I != -Inf)
# Output datasets in .dta format
write.dta(df, '../../data/dta/event_study_simulated_no_effect.dta')
write.dta(df2, '../../data/dta/event_study_simulated_with_effect.dta')
# Output datasets in .RDS format
saveRDS(df, '../../data/rds/event_study_simulated_no_effect.RDS')
saveRDS(df2, '../../data/rds/event_study_simulated_with_effect.RDS')
# To keep all graphs at the same scale
max_raw = max(df$newInfected)
max_log = log(max_raw)
max_rate = max(c(max(df$delta_log_I, na.rm = T), max(df2$delta_log_I, na.rm = T)))
min_rate = min(c(min(df$delta_log_I, na.rm = T), min(df2$delta_log_I, na.rm = T)))
g7 <- ggplot(df) +
aes(x = time, y = newInfected, color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('Raw') +
ylim(0, max_raw) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g8 <- ggplot(df) +
aes(x = time, y = log(newInfected), color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('Logs') +
ylim(0, max_log) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g9 <- ggplot(df) +
aes(x = time, y = delta_log_I, color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('Growth Rates') +
ylim(min_rate, max_rate) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g10 <- ggplot(df2) +
aes(x = time, y = newInfected, color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('') +
ylim(0, max_raw) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g11 <- ggplot(df2) +
aes(x = time, y = log(newInfected), color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('') +
ylim(0, max_log) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
g12 <- ggplot(df2) +
aes(x = time, y = delta_log_I, color = dep, group = dep) +
geom_line() +
xlab('') +
ylab('') +
ylim(min_rate, max_rate) +
theme_bw() +
scale_color_grey() +
theme(legend.position = "None",
legend.title = element_text(size = 15),
legend.text = element_text(size = 14),
axis.text=element_text(size=14),
axis.title=element_text(size=14))
ggarrange(col1, col2, g7, g10, g8, g11, g9, g12,
ncol = 2,
nrow = 4,
align = 'v',
common.legend = F,
widths = c(1,1),
heights = c(1,5,5,5)) +
ggsave('../../graphs/simulated_data_event_study.png', width = 25, height = 30, units = "cm")
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
fe
plot(sort(fe))
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
source('3_sc_empirical_analysis.R')
source('4_real_data_analysis.R')
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
source('3_sc_empirical_analysis.R')
source('4_real_data_analysis.R')
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
source('3_sc_empirical_analysis.R')
source('4_real_data_analysis.R')
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
source('3_sc_empirical_analysis.R')
source('4_real_data_analysis.R')
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
source('3_sc_empirical_analysis.R')
source('4_real_data_analysis.R')
# Main script to replicate our results.
# Will take a few minutes to run.
# We assume the working directory is set to "./codes/R"
# R Packages to install (run only once)
# install.packages('readr')
# install.packages('foreign')
# install.packages('lubridate')
# install.packages('reshape2')
# install.packages('xtable')
# install.packages('dplyr')
# install.packages('ggplot2')
# install.packages('ggpubr')
# install.packages('gsynth')
rm(list = ls())
set.seed(123)
# Run R scripts
source('simulation_functions.R')
source('1_make_illustrative_examples.R')
source('2_make_simulations.R')
source('3_sc_empirical_analysis.R')
source('4_real_data_analysis.R')
