**"COVID 19: Reduced forms have gone viral, but what do they tell us?", Léa Bou Sleiman & Germain Gauthier (2020)**

**Replication Folder Details**

Scripts are written in R. The "main.R" script runs all R scripts. Data is simulated based on the "simulation_functions.R" script. Our simulated datasets are provided in .dta and .RDS format for users interested in testing other specifications. The names of the datasets in "data" are self-explanatory.

For any further information or comments, feel free to drop us a line at: germain.gauthier[at]polytechnique.edu

Versions used:
1. R version 3.6.3

```
|-- Main Repository
    |-- codes
        |-- R
            |-- 1_make_illustrative_examples.R
            |-- 2_make_simulations.R
            |-- 3_empirical_analysis.R
            |-- 4_real_data_analysis.R
            |-- main.R
            |-- simulation_functions.R
    |-- data
        |-- dta
        |-- rds
        |-- all-states-history.csv
    |-- graphs
    |-- tables
```
